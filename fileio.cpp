/*
 *  Klasse FileIO ist f�r alle Interaktionen mit Dateien zust�ndig
 *
 *  -
 *
*/
#include "fileio.hpp"


FileIO::FileIO(QObject *parent) : QObject(parent)
{

}


//�ffnet eine Datei "FileName", liest diese als Text aus und gibt diesen mit "TempStringIn" zur�ck

QString FileIO::OpenFile(QString FileName)
{

    QFile openFile(FileName);  

    openFile.open(QFile::ReadOnly | QFile::Text); 
    QTextStream TempIn(&openFile);              

    QString TempStringIn = TempIn.readAll();    
    openFile.close();

    return TempStringIn;

}

bool FileIO::SaveAssFile(QString FileName,QString saveText)
{
    QFile saveFile(FileName);

    //Kommentar
    saveFile.open(QFile::WriteOnly | QFile::Text);

    //Kommentar
    QTextStream TempOut(&saveFile);

    //Kommentar
    // saveText >> TempOut;
    TempOut << saveText;
    saveFile.close();

    return true;
}

