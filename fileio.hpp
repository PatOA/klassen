#ifndef FILEIO_H
#define FILEIO_H

#include <QObject>
#include <QFile>
#include <QTextStream>

class FileIO : public QObject
{
    Q_OBJECT
public:
    explicit FileIO(QObject *parent = 0);
     QString OpenFile(QString FileName);
     bool SaveAssFile(QString FileName, QString saveText);

private:

    bool SaveFile(QString FileName);

};

#endif // FILEIO_H
